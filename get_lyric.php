<?php
ini_set("soap.wsdl_cache_enabled","0");
header('Content-Type: application/json');

class SearchLyric
{
    public $lyricId; 
    public $lyricCheckSum; 
}

try{

  $sClient = new SoapClient('http://api.chartlyrics.com/apiv1.asmx?WSDL');
  $str_json = file_get_contents('php://input');
  $parametros = json_decode ($str_json);
  $send = new SearchLyric();
  $send->lyricId = $parametros -> IdLyric;
  $send->lyricCheckSum = $parametros -> Checksum;
  $result = $sClient->GetLyric($send);
  echo json_encode($result);

  // Get the necessary parameters from the request
  // Use $sClient to call the operation GetLyric
  // echo the returned info as a JSON object

  //header(':', true, 501); // Just remove this line to return the successful 
                          // HTTP-response status code 200.
  //echo json_encode(array('Result' => 'Not implemented'));

}
catch(SoapFault $e){
  header(':', true, 500);
  echo json_encode($e);
}

