<?php
ini_set("soap.wsdl_cache_enabled","0");
header('Content-Type: application/json');

class SearchLyricText
{
    public $lyricText; 
}


try{

  $sClient = new SoapClient('http://api.chartlyrics.com/apiv1.asmx?WSDL');
  $str_json = file_get_contents('php://input');
  $parametros = json_decode ($str_json);
  $send = new SearchLyricText();
  $send->lyricText = $parametros -> searchText;
  $result = $sClient->SearchLyricText($send);
  echo json_encode($result);
// return $parametros -> searchText;
  
  // Get the necessary parameters from the request
  // Use $sClient to call the operation SearchLyricText
  // echo the returned info as a JSON array of objects

  //header(':', true, 501); // Just remove this line to return the successful 
                          // HTTP-response status code 200.
  //echo '["Not","Yet","Implemented"]';
  
}
catch(SoapFault $e){
  header(':', true, 500);
  echo json_encode($e);
}

function compare_some_objects($a, $b) { // Make sure to give this a more meaningful name!
  return $a->Song - $b->Song;
}
?>
