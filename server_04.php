<?php
 
ini_set("soap.wsdl_cache_enabled","0");
$server = new SoapServer("http://localhost:8080/waslab04/WSLabService.wsdl");

class ConversionRequest
{
    public $from_Currency; 
    public $to_Currencies;
    public $amount;
}

class ConversionResult
{
    public $currency; 
    public $amount;
}

function FahrenheitToCelsius($fdegree){
    $cresult = ($fdegree - 32) * (5/9);
    return array("cresult"=> $cresult, "timeStamp"=> date('c', time()) );
}

function CurrencyConverter($from_Currency,$to_Currency,$amount) {
	$uri = "http://currencies.apps.grandtrunk.net/getlatest/$from_Currency/$to_Currency";
	$rate = doubleval(file_get_contents($uri));
	return round($amount * $rate, 2);
};

function CurrencyConverterPlus($ConversionRequest) 
    {
    //error_log($ConversionRequest -> to_Currencies,0)
    $from_Currency = $ConversionRequest -> from_Currency;
    $to_Currencies = array();
    $to_Currencies = $ConversionRequest -> to_Currencies;
    $amount = $ConversionRequest -> amount;
    error_log($from_Currency, 0);
    error_log($to_Currencies[0], 0);
    error_log($amount, 0);
    $ConverArray = array();
    for ($i = 0; $i < count($to_Currencies); ++$i){
    
        $result = new ConversionResult();
        $result-> currency  = $to_Currencies[$i];
        $result-> amount =  CurrencyConverter($from_Currency,$to_Currencies[$i], $amount);
        array_push($ConverArray,$result);
        
    }
  
	return $ConverArray;
};


// Task #4: Implement here the CurrencyConverterPlus function and add it to $server
$server->addFunction("CurrencyConverterPlus");

$server->addFunction("FahrenheitToCelsius");

// Task #3 -> Uncomment the following line:
$server->addFunction("CurrencyConverter");

$server->handle();
 
?>
