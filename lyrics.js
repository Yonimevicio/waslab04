var baseURI = "http://localhost:8080/waslab04";
var URI_Search = baseURI + "/lyric_search.php";
var URI_GetLyric = baseURI + "/get_lyric.php";
var req;

function showSongs() {
   var content = document.getElementById("content").value;
    
   // Replace the two lines below with your implementation
           
    var searchText = document.getElementById("content").value;
	req = new XMLHttpRequest();
	req.open('POST', URI_Search, /*async*/true);
	req.onreadystatechange = function() {
             
		if (req.readyState == 4 && req.status == 200) {
				var canciones = JSON.parse(req.responseText).SearchLyricTextResult.SearchLyricResult;
                document.getElementById("left").innerHTML = "<ul>";
                canciones.forEach(function(cancion){
                   if (cancion != null){
                     var link = "<li><a href='#' onclick = 'getLyric(\""+cancion.LyricId+"\",\""+cancion.LyricChecksum+"\")'>"+ cancion.Song +"</a>("+ cancion.Artist +")</li>";
                     document.getElementById("left").innerHTML += link;   
                    }
                });
                  document.getElementById("left").innerHTML += "</ul>";
                  
                   
		}
	}
    req.setRequestHeader("Content-Type", "application/json");
	req.send(/*no params*/JSON.stringify({searchText:searchText}));

};

function getLyric (id,checksum) {

  // Your implementation
   req = new XMLHttpRequest();
   req.open('POST', URI_GetLyric, /*async*/true);
   req.onreadystatechange = function() {
		if (req.readyState == 4 && req.status == 200) {
				var cancion = JSON.parse(req.responseText).GetLyricResult;
                console.log(cancion);
                 document.getElementById("right").innerHTML = "<div id='title'><h2>"+ cancion.LyricSong +"</h2><h3>"+ cancion.LyricArtist +"</h3></div><img id='portada'  src='"+cancion.LyricCovertArtUrl+"'></img>  <div style='clear: both'></div><em>"+cancion.Lyric+"</em>";          
		}
	}
    req.setRequestHeader("Content-Type", "application/json");
	req.send(/*no params*/JSON.stringify({IdLyric:id,Checksum:checksum}));
};

window.onload = showSongs();

