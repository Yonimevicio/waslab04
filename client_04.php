<?php

header("Content-Type: text/plain");
ini_set("soap.wsdl_cache_enabled","0");
date_default_timezone_set("Europe/Andorra");

class ConversionRequest
{
    public $from_Currency; 
    public $to_Currencies;
    public $amount;
}

class ConversionResult
{
    public $currency; 
    public $amount;
}

class ArrayOfConversionResult
{
    public $conversionResult; 
}

try{
    
	$sClient = new SoapClient('http://localhost:8080/waslab04/WSLabService.wsdl', array('trace' => 1));
	$fahrenheitTemp = "63.5";
	$response = $sClient->FahrenheitToCelsius($fahrenheitTemp);

	echo "\n   ";
	echo $fahrenheitTemp, ' Fahrenheit ==> ', $response["cresult"], ' Celsius ';
	echo "[Server TimeStamp: ", date('l jS \of F Y @ h:i:s A',strtotime($response["timeStamp"])), "]\n";

	/* Task #2: Write your code here. Use the function xmlpp (implemented below)
	 * to print the 2 SOAP messages (request and response).*/
	echo "Request header:\n " . $sClient->__getLastRequestHeaders(). "\n";
	echo "Last Request:\n " . xmlpp($sClient->__getLastRequest()). "\n";
	echo "Last Response header:\n " . $sClient->__getLastResponseHeaders(). "\n";
	echo "Last Response :\n " . xmlpp($sClient->__getLastResponse()). "\n";
	
	// Task #3: Uncomment the following lines:
    $inCur = "EUR";
	$outCur = "CNY";
	$inAmount = 100;
	$outAmount = $sClient->CurrencyConverter($inCur,$outCur,$inAmount);
	echo "\n   ", $inAmount, " ",$inCur, " ==> ",$outAmount, " ",$outCur,"\n\n";
	
	
	// Task #4: Call CurrencyConverterPlus and display its result:
	$outCur2 = ["CNY","AED","ALL","AMD"];
	$cr = new ConversionRequest();
	
	$cr-> from_Currency = $inCur;
	$cr-> to_Currencies = $outCur2;
	$cr-> amount = $inAmount;
	
	

	echo "\n" . $cr-> from_Currency . "\n";
	echo $cr-> to_Currency[0] . "\n";
	echo $cr-> amount . "\n";
	
	$outAmount = new ArrayOfConversionResult();
	$outAmount = $sClient->CurrencyConverterPlus($cr);
    
    for ($i = 0; $i < count($outAmount);++$i)
        {
        echo "\n   ", $inAmount, " ",$inCur, " ==> ",$outAmount[$i]->amount, " ",$outAmount[$i]->currency,"\n\n";
        }
	
} catch(SoapFault $e){
	echo "ERROR";
	var_dump($e);
}

// ---------------------------------------------------------------------------
// Function xmlpp prints a xml-formatted string ($xml) with a proper nesting
function xmlpp($xml) {  
    $xml_obj = new SimpleXMLElement($xml);  
    $level = 4;  
    $indent = 0; // current indentation level  
    $pretty = array();  
      
    // get an array containing each XML element  
    $xml = explode("\n", preg_replace('/>\s*</', ">\n<", $xml_obj->asXML()));  
  
    // shift off opening XML tag if present  
    if (count($xml) && preg_match('/^<\?\s*xml/', $xml[0])) {  
      $pretty[] = array_shift($xml);  
    }  
  
    foreach ($xml as $el) {  
      if (preg_match('/^<([\w])+[^>\/]*>$/U', $el)) {  
          // opening tag, increase indent  
          $pretty[] = str_repeat(' ', $indent) . $el;  
          $indent += $level;  
      } else {  
        if (preg_match('/^<\/.+>$/', $el)) {              
          $indent -= $level;  // closing tag, decrease indent  
        }  
        if ($indent < 0) {  
          $indent += $level;  
        }  
        $pretty[] = str_repeat(' ', $indent) . $el;  
      }  
    }     
    $xml = implode("\n", $pretty);     
    return $xml;  
}  
?>
